# Git pour Windows

![alt text](images/git_bash.png)

#### **Situation** : **Linux** c'est l'été dans *Game of Thrones* mais voilà... *Winter is coming* lorsque vous rentrez chez vous et que vous êtes sous **Windows**...
**Traduction** : comment **installer Git sous Windows** ?

* Allez sur [Git for Windows](https://gitforwindows.org/) pour télécharger Git
* Une fois le fichier obtenu (ici : "*Git-2.19.1-64-bit.exe*"), installez-le. La seule difficulté c'est le nombre d'options disponibles lors de l'installation... Vous pouvez laisser tout par défaut, cela devrait marcher. Veillez à choisir votre éditeur préféré lors de "Choosing the default editor used by Git" si vous tenez à avoir le vôtre (un conseil, Notepad++ est très pratique mais vous avez une belle liste)

![alt text](images/choose_editor.jpg)

* Vous devriez avoir maintenant l'option "Git Bash Here" avec le clic droit dans votre navigateur de fichiers

![alt text](images/git_bash_contextuel.jpg)

* Cela ouvre un terminal semblable à celui de Linux, vous pouvez alors utiliser Git et vous amuser à *commit*, *push* et *pull* !

![alt text](images/git_bash_window.jpg)

*Mais oui, jeune padawan, nous n'avons pas encore évoqué le *pull*, je vois que tu as hâte d'apprendre...*