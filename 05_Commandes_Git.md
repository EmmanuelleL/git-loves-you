# Commandes Git

### Parce que vous êtes un-e pro et que les tutoriels, ce n'est plus pour vous


#### La base

* **```git add```** : pour ajouter des fichiers avant de faire un **```commit```**, pour ajouter tout le contenu du répertoire faites un **```git add .```** mais si vous ne souhaitez ajouter que quelques fichiers, faites **```git add <nom_de_fichier>```**

* **```git commit```** : après avoir **add** vos fichiers, vous pouvez les empaqueter pour un **push**, un petit nom et c'est presque fini !

* **```git push```** : enfin vous envoyez vos changements sur le serveur, n'oubliez pas que dans certains cas, vous devrez ajouter **origin master** sur la branche a été créée manuellement comme dans le tutoriel 02

* **```git pull```** : mettre à jour votre dépôt depuis le serveur


#### Commandes avancées

* **```git init```** : initialiser un dépôt Git en local pour le lier à un dépôt GitLab par la suite

* **```git status```** : permet de voir les changements actuels (quels fichiers ont été modifiés ?)

* **```gitk```** : ouvre une interface vous permettant de voir tous les **commits** que vous avez fait, ainsi que les branches et les modifications (ajoutées ou enlevées)

* **```git remote add origin <url>```** : met en place un lien entre votre dépôt local et votre dépôt GitLab, cela demande d'avoir d'avoir fait un ```git init```

* **```git log```** : affiche tous les *commits* effectués (appuyez sur "Entrée" pour descendre et sur "Q" pour quitter)

#### Les *branchs*

* **```git branch <nom>```** : ajouter une nouvelle branche à votre dépôt pour travailler sans modifier la branche principale, pratique pour travailler à deux

* **```git checkout <nom>```** : pour changer de branche

* **```git merge <branch_name>```**

* **```git branch -u origin/master```** : pour dire à la *branch master* de suivre les changements d'*origin* (utilisé après un **```git remote add origin <url>```**)

* **```git branch -vv```** : pour savoi si la *branch* est bien paramétrée pour suivre l'*origin* par exemple


#### Revenir sur des commits précédents

* **```git revert <code_sha>````** : si vous souhaitez revenir sur un commit précédent, vous pouvez le faire en utilisant ```git log```, le code SHA est affiché pour chaque commit. Il suffit de le copier-coller avec la commande ```git revert```

* **```git reset```** : extrêment dangereux, ne pas toucher. En vrai, c'est utilisé pour supprimer les fichiers de l'index (aussi appelé stage) qui sont ajoutés par ```git add```


# Astuces
* **Pas de git-ception** : Ne tentez pas de faire un dépôt git si un sous-dossier a déjà un dépôt git, cela bouleversera git à tel point qu'il peut détruire l'espace-temps. Pour vérifier si ce n'est pas le cas, vous pouvez appuyer sur **Ctrl+H** et vous verrez s'il y a des répertoires ".git" dans les sous-dossiers, que vous supprimerez bien vite !

* **Clonez dans un dossier sandbox** : Lorsque vous clonez un dépôt *git* depuis GitLab, faites le dans un dossier fait pour ça, appelé **git** par exemple. Ainsi, vous pourrez travailler dessus et une fois que ce sera terminé, vous pourrez l'ajouter à vos archives en supprimant le **.git** si vos archives sont liées à un dépôt *git*

* **Soyez git** : vous pouvez lier un dossier contenant tout votre travail à la Code Académie à un dépôt GitLab, ainsi vous y avez accès chez vous et il vous suffit de ***push*** et de ***pull*** pour envoyer vos modifications et vous mettre à jour ! Fini les clés USB et les salons Discord pour partager des fichiers !

* **Travaillez avec tous vos amis** : partagez votre dépôt GitLab entre tous vos meilleur-es ami-es pour que tout le monde s'amuse ! Ajoutez simplement des membres à votre dépôt : ***Settings/Members***  
**Mettez les nouveaux members avec le rôle de "Maintainer" si vous souhaitez qu'iels puissent travailler directement sur le dépôt GitLab**

![alt text](images/members.png) ![alt text](images/add_members.png)