# Forker un dépôt GitLab

#### **Situation** : Oh ! Un nouveau challenge ! Chouette ! Mais comment travailler dessus ?

* Choisir "Fork" sur GitLab pour créer votre propre dépôt à partir du dépôt maître

![alt text](images/fork.jpg)

* Ainsi vous pouvez cloner votre nouveau dépôt en local pour travailler dessus. Copiez l'url du dépôt GitLab que vous avez forké puis, dans le terminal, entrez la commande suivante **```git clone <url>```**

![alt text](images/copy_url.jpg)

* Après votre session de travail, il suffira de vous placer dans votre dossier avec le terminal, avec **```cd```** ou en ouvrant le terminal directement avec un clic droit dans le navigateur de fichiers. Vous entrez la commande **```git add .```** pour ajouter tous les fichiers contenus dans le dossier ou **```git add <nom_du_fichier>```** pour ajouter un fichier spécifique. Il est possible d'ajouter plusieurs fichiers si vous le souhaitez, tant que le *commit* n'est pas encore fait
* Vous avez ajouté les fichiers souhaités, maintenant il faut les *commit*, c'est-à-dire prendre tous les fichiers ajoutés par la commande **```git add```** pour les placer dans un panier virtuel avant de les envoyer. Dans le terminal, entrez **```git commit```** et cela vous ouvrira une nouvelle interface où il vous sera demandé d'écrire quelque chose.
* Il s'agit de nommer votre commit, ce nom sera visible sur GitLab avec l'heure à laquelle vous l'avez fait et ce que vous avez modifié. Une fois un nom choisi faites **Ctrl + X** pour quitter, appuyez sur **O** pour accepter d'écrire puis **Entrée** pour valider le nom du fichier.

![alt text](images/commit.jpg)

* Votre *commit* est fait, il ne reste plus qu'à *push*, donc à pousser les modifications que vous avez faites vers le serveur (GitLab). Sur le terminal, tapez **```git push```** et *git* enverra votre *commit* vers GitLab

![alt text](images/ensemble_commandes.jpg)

* Allez sur GitLab et vous devriez voir votre dernier *commit*, cliquez dessus pour voir les modifications

# Synthèse

```sh
git clone <url>
git add .
git commit
git push
```